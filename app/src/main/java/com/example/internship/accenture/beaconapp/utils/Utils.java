package com.example.internship.accenture.beaconapp.utils;

import android.content.Context;
import android.provider.Settings;

import com.example.internship.accenture.beaconapp.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public final class Utils {
    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

    public static void logOut(String userID) {
        DatabaseReference database;
        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
        database.child(userID).child(Constants.ONLINE).setValue(false);
    }


}
