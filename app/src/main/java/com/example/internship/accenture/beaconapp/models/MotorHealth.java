package com.example.internship.accenture.beaconapp.models;

import com.example.internship.accenture.beaconapp.Constants;
import com.google.gson.annotations.SerializedName;


public class MotorHealth {

    @SerializedName(Constants.ASSET)
    private String asset;

    @SerializedName(Constants.IS_LIVE)
    private String isLive;

    @SerializedName(Constants.FREQUENCY)
    private String frequency;

    @SerializedName(Constants.HEALTH)
    private String health;

    public MotorHealth(String asset, String isLive, String frequency, String health) {
        this.asset = asset;
        this.isLive = isLive;
        this.frequency = frequency;
        this.health = health;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }
}
