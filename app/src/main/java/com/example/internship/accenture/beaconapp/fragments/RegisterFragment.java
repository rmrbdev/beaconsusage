package com.example.internship.accenture.beaconapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.internship.accenture.beaconapp.Constants;
import com.example.internship.accenture.beaconapp.MainActivity;
import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class RegisterFragment extends Fragment {


    private EditText userNameEditText;
    private Button submitButton;


    private DatabaseReference database;
    private SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        initFirebase();
        initSharedPreferances();

        View view = initUI(inflater, container);

        pressButton();

        return view;
    }

    private void initFirebase() {
        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
    }

    private View initUI(LayoutInflater inflater, @Nullable ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        userNameEditText = view.findViewById(R.id.nameField);
        submitButton = view.findViewById(R.id.submitButton);
        return view;
    }


    private void pressButton() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionOfButton();
            }
        });
    }


    private void actionOfButton() {
        database.addListenerForSingleValueEvent(new ValueEventListener() {

            List<User> userList = new ArrayList<>();
            String userName = userNameEditText.getText().toString();

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String userID = "";
                String userOcupation = "";
                if (!TextUtils.isEmpty(userName)) {
                    if (userName.matches(Constants.USERNAME_VALIDATION_PATTERN)) {
                        getUsersFromFirebase(dataSnapshot, userList);

                        if (searchUser(userName, userList).size() != 0) {
                            userID += searchUser(userName, userList).get(0);
                            userOcupation += searchUser(userName, userList).get(1);
                            loginUser(userID, userName, userOcupation);
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), Constants.USERNAME_NOT_EXIST, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), Constants.BAD_USERNAME, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), Constants.ADD_USERNAME, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("tag", "onCancelled: ERROR");
            }
        });
    }


    private void initSharedPreferances() {
        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
    }


    private void saveToSharedPreferences(@Nullable String userID, @Nullable String userName, @Nullable String userOcupation) {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(Constants.USER_NAME, userName);
        editor.putString(Constants.USER_ID, userID);
        editor.putString(Constants.USER_OCUPATION, userOcupation);
        editor.apply();
    }



    private void loginUser(String userID, String userName, String userOcupation) {
        database.child(userID).child(Constants.ONLINE).setValue(true);
        saveToSharedPreferences(userID, userName, userOcupation);
        startMainActivity();
    }

    private ArrayList<String> searchUser(String userName, List<User> userList) {
        ArrayList<String> userData = new ArrayList<>();
        for (User user : userList) {
            if (user.getUsername().equals(userName)) {
                userData.add(user.getUserID());
                userData.add(user.getOccupation());
                break;
            }
        }
        return userData;
    }


    private void getUsersFromFirebase(DataSnapshot dataSnapshot, List<User> userList) {
        userList.clear();
        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
            User user = userSnapshot.getValue(User.class);
            userList.add(user);
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

}
