package com.example.internship.accenture.beaconapp;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.internship.accenture.beaconapp.utils.BluetoothReceiver;
import com.example.internship.accenture.beaconapp.utils.NetworkUtils;
import com.example.internship.accenture.beaconapp.utils.Utils;


public class ActivityManager extends AppCompatActivity {

    private BroadcastReceiver bluetoothReceiver;
    private BroadcastReceiver networkReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onResume() {
        super.onResume();

        networkReceiver = new NetworkUtils();
        bluetoothReceiver = new BluetoothReceiver();
        registerNetworkBroadcast();
        registerBluetooth();

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled() && !Utils.isAirplaneModeOn(this)) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterNetworkBroadcast();
        unregisterBluetooth();
    }


    public void registerBluetooth() {
        registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    public void unregisterBluetooth() {
        try {
            unregisterReceiver(bluetoothReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    private void registerNetworkBroadcast() {
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterNetworkBroadcast() {
        try {
            unregisterReceiver(networkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
