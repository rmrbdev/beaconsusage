package com.example.internship.accenture.beaconapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.internship.accenture.beaconapp.models.User;
import com.example.internship.accenture.beaconapp.utils.AddUserDialog;
import com.example.internship.accenture.beaconapp.utils.ModifyUserDialog;
import com.example.internship.accenture.beaconapp.utils.SearchAdapter;
import com.example.internship.accenture.beaconapp.utils.UserViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class TeamActivity extends ActivityManager {

    private FirebaseRecyclerAdapter<User, UserViewHolder> adapter;
    private SearchAdapter searchAdapter;
    private DatabaseReference database;
    RecyclerView rvUsers;
    private List<User> userList;

    private String userOccupation;
    private SharedPreferences sharedPreferences;

    private Paint p = new Paint();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSharedPreferances();
        userOccupation = sharedPreferences.getString(Constants.USER_OCUPATION, "");
        userList = new ArrayList<>();
        initUI();
        setUsers();
        initSwipe();
    }


    private void setUsers() {

        if (!userList.isEmpty()) {
            userList.clear();
        }

        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
        adapter = new FirebaseRecyclerAdapter<User, UserViewHolder>(
                User.class,
                R.layout.item_layout, UserViewHolder.class, database

        ) {
            @Override
            protected void populateViewHolder(UserViewHolder viewHolder, User model, int position) {
                viewHolder.nameTextView.setText(model.getUsername());
                if (model.isOnline())
                    viewHolder.imageView.setImageResource(R.mipmap.green);
                else {
                    viewHolder.imageView.setImageResource(R.mipmap.red);

                }
                userList.add(model);
            }
        };
        rvUsers.setAdapter(adapter);
    }


    private void initSwipe() {
        if (userOccupation.equals("HR")) {
            ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = viewHolder.getAdapterPosition();

                    User modUser = adapter.getItem(position);
                    String userID = modUser.getUserID();
                    String username = modUser.getUsername();
                    String occupation = modUser.getOccupation();
                    boolean isOnline = modUser.isOnline();

                    if (direction == ItemTouchHelper.RIGHT) {
                        showModifyDialog(userID, isOnline, username, occupation);
                    } else {
                        showConfirmDialog(userID, username);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                    Bitmap icon;
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;

                        if (dX > 0) {
                            p.setColor(Color.parseColor("#388E3C"));
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_create_white);
                            RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        } else {
                            p.setColor(Color.parseColor("#D32F2F"));
                            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                            RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        }
                    }
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
            itemTouchHelper.attachToRecyclerView(rvUsers);
        }
    }

    public void showModifyDialog(String userID, boolean isOnline, String username, String occupation) {
        ModifyUserDialog modifyUserDialog = new ModifyUserDialog();
        modifyUserDialog.setUsers(userList);
        modifyUserDialog.setUserID(userID);
        modifyUserDialog.setOnline(isOnline);
        modifyUserDialog.setUsername(username);
        modifyUserDialog.setOccupation(occupation);
        modifyUserDialog.show(getSupportFragmentManager(), Constants.MODIFY_USER);
    }

    public void showConfirmDialog(final String userID, final String userName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete " + userName + "?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        database.child(userID).removeValue();
                        Toast.makeText(TeamActivity.this, "User " + userName + "has been deleted successfull", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void initSharedPreferances() {
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
    }


    private void initUI() {
        setContentView(R.layout.activity_team_screen);

        rvUsers = (RecyclerView) findViewById(R.id.recyclerviewTeam);
        // Set layout manager to position the items
        rvUsers.setLayoutManager(new LinearLayoutManager(this));
        rvUsers.hasFixedSize();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if (userOccupation.equals(Constants.HR)) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddUserDialog addUserDialog = new AddUserDialog();
                    addUserDialog.setUsers(userList);
                    addUserDialog.show(getSupportFragmentManager(), Constants.ADD_NEW_USER);
                }
            });
        } else {
            fab.setVisibility(View.INVISIBLE);
        }

    }


    // Implementing Searchview
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_bar, menu);
        MenuItem item = menu.findItem(R.id.item_search_bar);

        SearchView searchView = (SearchView) item.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    setUsers();
                } else {
                    String query = newText.toLowerCase();
                    List<User> filteredUserList = new ArrayList<>();
                    for (User user : userList) {
                        String username = user.getUsername().toLowerCase();
                        if (username.contains(query)) {
                            filteredUserList.add(user);
                        }
                    }
                    searchAdapter = new SearchAdapter(getApplicationContext(), filteredUserList);
                    rvUsers.setAdapter(searchAdapter);

                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onDestroy() {
        adapter.cleanup();
        super.onDestroy();
    }


}



