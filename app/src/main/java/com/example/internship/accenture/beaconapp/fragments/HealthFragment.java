package com.example.internship.accenture.beaconapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.internship.accenture.beaconapp.Constants;
import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.MotorHealth;
import com.example.internship.accenture.beaconapp.utils.MotorHealthAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HealthFragment extends Fragment {

    RelativeLayout containerLayout;
    TextView titleView;
    ImageView healthView;
    TextView frequencyView;
    TextView isAliveView;

    private String title;
    private String isLive;
    private String frequency;
    private String health;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        loadDataRetrofit();
        return initUI(inflater, container);
    }

    private View initUI(LayoutInflater inflater, @Nullable ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_health, container, false);

        containerLayout = view.findViewById(R.id.health_relativeLayout);
        titleView = view.findViewById(R.id.title_textView);
        healthView = view.findViewById(R.id.health_imageView);
        frequencyView = view.findViewById(R.id.frequency_textView);
        isAliveView = view.findViewById(R.id.isAlive_textView);
        return view;
    }

    private void loadDataRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.RETROFIT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MotorHealthAPI motorHealthAPI = retrofit.create(MotorHealthAPI.class);
        Call<MotorHealth> call = motorHealthAPI.getData();

        call.enqueue(new Callback<MotorHealth>() {
            @Override
            public void onResponse(@NonNull Call<MotorHealth> call, @NonNull Response<MotorHealth> response) {

                if (response.body() != null) {

                    setTextOnFields(response);
                    setTextOnActivity();

                    if (health.equals(Constants.COLOR_GREEN)) {
                        healthView.setImageResource(R.drawable.greenimg);
                    } else if (health.equals(Constants.COLOR_RED)) {
                        healthView.setImageResource(R.drawable.redimg);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MotorHealth> call, @NonNull Throwable t) {
            }
        });
    }


    private void setTextOnFields(@NonNull Response<MotorHealth> response) {
        title = response.body().getAsset();
        isLive = "Is live: " + response.body().getIsLive();
        frequency = "Frequency: " + response.body().getFrequency();
        health = response.body().getHealth();
    }

    private void setTextOnActivity() {
        containerLayout.setBackgroundResource(R.drawable.layout_bg);
        titleView.setText(title);
        frequencyView.setText(frequency);
        isAliveView.setText(isLive);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
