package com.example.internship.accenture.beaconapp;


public final class Constants {
    public final static String USERS = "users";
    public final static String ONLINE = "online";
    public final static String USER_ID = "UserID";
    public final static String USER_NAME = "UserName";
    public final static String USER_OCUPATION = "UserOcupation";
    public final static String USERNAME_NOT_EXIST = "Username not exist";
    public final static String YOUTUBE_KEY = "AIzaSyA5HfYdpzidpEzI_1C_x-eIxmfTbDkhw4g";
    public final static String YOUTUBE_VIDEO_URL = "4W_NRHxekaY";
    public static final String RETROFIT_URL = "http://www.mocky.io/";
    public final static String USERNAME_VALIDATION_PATTERN = "[A-Z][a-z]*[ ][A-Z][a-z]*";

    public final static String USERNAME_EXISTS = "Username already exists";
    public final static String BAD_USERNAME = "Username is not correct!\nexample: John Snow";
    public final static String ADD_USERNAME = "You have to enter a name";
    public final static String LOGOUT = "Logout";
    public final static String LOGOUT_QUESTION = "Are you sure you want to logout?";
    public final static String SHARED_PREF_FILE = "LoginFile";
    public final static String ADD_NEW_USER = "Add new user";
    public final static String MODIFY_USER = "Update user data";
    public final static String SELECT_POSITION = "Select position";
    public final static String UPDATE_SUCCESSFULLY = "User data has been updated successfully";

    public final static String COLOR_GREEN = "green";
    public final static String COLOR_RED = "red";

    public final static String ADD = "ADD";
    public final static String UPDATE = "UPDATE";
    public final static String CANCEL = "CANCEL";
    public final static String DISCONNECTED = "disconnected";

    public final static String YES = "YES";
    public final static String NO = "NO";
    public final static String HR = "HR";


    public final static String ONYX_SECRET = "c1811f0a4199c0dc006a20b86984f01ac03f89ea";
    public final static String ONYX_CLIENT_ID = "44a56c79909ece72a54b561a1a333efcdaee3da8";
    public final static String ONYX_APY_ENDPOINT = " https://connect.onyxbeacon.com ";
    public final static String ONYX_CONTENT_RECEIVER = "com.example.internship.accenture.beaconapp.content";

    public final static String ASSET = "asset";
    public final static String IS_LIVE = "isLive";
    public final static String FREQUENCY = "frequency";
    public final static String HEALTH = "health";

}
