package com.example.internship.accenture.beaconapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.internship.accenture.beaconapp.R;
import com.skyfishjy.library.RippleBackground;


public class SearchFragment extends Fragment {

    RippleBackground rippleBackground;

    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return initUI(inflater, container);
    }


    private View initUI(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        rippleBackground = view.findViewById(R.id.ripple);
        rippleBackground.startRippleAnimation();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


}
