package com.example.internship.accenture.beaconapp;


public final class Flags {
    public static boolean healthFragmentAppeared = false;
    public static boolean videoFragmentAppeared = false;
    public static boolean logoutFragmentAppeared = false;
    public static boolean searchSignalFragmentAppeared = false;
    public static boolean onStoppedFlag = false;
}
