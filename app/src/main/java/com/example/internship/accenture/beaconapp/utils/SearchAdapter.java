package com.example.internship.accenture.beaconapp.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.User;

import java.util.ArrayList;
import java.util.List;


public class SearchAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private final LayoutInflater inflater;
    private List<User> userList;


    public SearchAdapter(Context context, List<User> users) {
        inflater = LayoutInflater.from(context);
        userList = new ArrayList<>(users);
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = inflater.inflate(R.layout.item_layout, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final User user = userList.get(position);
        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }


}
