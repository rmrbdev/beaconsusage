package com.example.internship.accenture.beaconapp.models;


public class User {

    private String userID;
    private String username;
    private boolean isOnline;
    private String occupation;

    public User() {
    }

    public User(String userID, String username, boolean isOnline, String workAs) {
        this.userID = userID;
        this.username = username;
        this.isOnline = isOnline;
        this.occupation = workAs;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }


}
