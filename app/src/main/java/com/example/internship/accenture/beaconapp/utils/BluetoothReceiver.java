package com.example.internship.accenture.beaconapp.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class BluetoothReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String mExtraState = BluetoothAdapter.EXTRA_STATE;

        int state = intent.getIntExtra(mExtraState, -1);

        switch (state) {
            case (BluetoothAdapter.STATE_TURNING_ON):
                break;
            case (BluetoothAdapter.STATE_ON):
                break;
            case (BluetoothAdapter.STATE_TURNING_OFF):
                break;
            case (BluetoothAdapter.STATE_OFF):
                if (!Utils.isAirplaneModeOn(context)) {
                    ((Activity) context).startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
                }
                break;
        }
    }
}
