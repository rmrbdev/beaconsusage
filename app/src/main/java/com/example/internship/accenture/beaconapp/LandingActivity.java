package com.example.internship.accenture.beaconapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;

import com.example.internship.accenture.beaconapp.fragments.RegisterFragment;
import com.example.internship.accenture.beaconapp.fragments.SearchFragment;
import com.example.internship.accenture.beaconapp.utils.ContentReceiver;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onyxbeacon.OnyxBeaconApplication;
import com.onyxbeacon.OnyxBeaconManager;
import com.onyxbeacon.listeners.OnyxBeaconsListener;
import com.onyxbeacon.rest.auth.util.AuthData;
import com.onyxbeacon.rest.auth.util.AuthenticationMode;
import com.onyxbeaconservice.Beacon;
import com.onyxbeaconservice.IBeacon;

import java.util.List;

public class LandingActivity extends ActivityManager implements OnyxBeaconsListener {

    private OnyxBeaconManager manager;
    private ContentReceiver contentReceiver;
    private DatabaseReference database;
    private SharedPreferences sharedPreferences;

    private boolean registerFragmentAppeared = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initOnyxManager();
        initUI(savedInstanceState);


    }

    private void initOnyxManager() {

        manager = OnyxBeaconApplication.getOnyxBeaconManager(this);
        manager.setAPIEndpoint(Constants.ONYX_APY_ENDPOINT);
        manager.setAPIContentEnabled(true);
        manager.enableGeofencing(true);
        manager.setLocationTrackingEnabled(true);

        AuthData authData = new AuthData();
        authData.setAuthenticationMode(AuthenticationMode.CLIENT_SECRET_BASED);
        authData.setClientId(Constants.ONYX_CLIENT_ID);
        authData.setSecret(Constants.ONYX_SECRET);
        manager.setAuthData(authData);

    }


    private void initUI(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_landing_screen);
        getSupportActionBar().hide();

        initFirebase();
        initSharedPreferances();
        loginWithSharedPreferences(savedInstanceState);


    }

    private void initMainFragment() {
        SearchFragment searchFragment = new SearchFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.container, searchFragment).addToBackStack(null).commit();
    }

    public void changeToRegisterFragments() {
        registerFragmentAppeared = true;

        Fragment registerFragment = new RegisterFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, registerFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    public void onResume() {
        super.onResume();
        manager.setForegroundMode(true);
        if (contentReceiver == null) contentReceiver = ContentReceiver.getInstance();
        contentReceiver.setOnyxBeaconsListener(this);
        this.registerReceiver(contentReceiver, new IntentFilter(Constants.ONYX_CONTENT_RECEIVER));
    }

    public void onPause() {
        super.onPause();
        manager.setForegroundMode(false);
        unregisterReceiver(contentReceiver);
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    private void initFirebase() {
        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
    }

    private void initSharedPreferances() {
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
    }

    private void loginWithSharedPreferences(Bundle savedInstanceState) {
        String username = sharedPreferences.getString(Constants.USER_NAME, "");
        String userid = sharedPreferences.getString(Constants.USER_ID, "");
        String userOcupation = sharedPreferences.getString(Constants.USER_OCUPATION, "");
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(userid) && !TextUtils.isEmpty(userOcupation)) {
            database.child(userid).child(Constants.ONLINE).setValue(true);
            startMainActivity();
        } else {
            if (findViewById(R.id.container) != null) {
                if (savedInstanceState != null) {
                    return;
                }

                initMainFragment();

            }
        }
    }


    @Override
    public void didRangeBeaconsInRegion(List<Beacon> list) {
        for (Beacon beacon : list) {
            if (beacon instanceof IBeacon) {
                IBeacon iBeacon = (IBeacon) beacon;
                if (iBeacon.getMinor() == 6657 && !registerFragmentAppeared) {
                    changeToRegisterFragments();

                }
            }
        }
    }
}

