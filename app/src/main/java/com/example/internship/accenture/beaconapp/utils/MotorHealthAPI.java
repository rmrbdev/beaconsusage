package com.example.internship.accenture.beaconapp.utils;

import com.example.internship.accenture.beaconapp.models.MotorHealth;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MotorHealthAPI {

    @Headers("Content-Type: application/json")
    @GET("v2/598c6fe12600001007ec22c1")
    Call<MotorHealth> getData();
}
