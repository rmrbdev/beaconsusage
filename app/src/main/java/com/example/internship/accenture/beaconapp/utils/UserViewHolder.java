package com.example.internship.accenture.beaconapp.utils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.User;


public class UserViewHolder extends RecyclerView.ViewHolder {

    public TextView nameTextView;
    public ImageView imageView;

    public UserViewHolder(View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.textViewName);
        imageView = itemView.findViewById(R.id.imageView2);
    }

    public void bind(User user) {
        nameTextView.setText(user.getUsername());
        if (user.isOnline())
            imageView.setImageResource(R.mipmap.green);
        else {
            imageView.setImageResource(R.mipmap.red);
        }
    }

}