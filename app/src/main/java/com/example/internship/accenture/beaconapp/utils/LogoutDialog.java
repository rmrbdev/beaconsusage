package com.example.internship.accenture.beaconapp.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.example.internship.accenture.beaconapp.Constants;


public class LogoutDialog extends DialogFragment {


    private String userID;


    public void setUserID(String userID) {
        this.userID = userID;
    }


    private DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            Utils.logOut(userID);
            getActivity().finish();
        }
    };

    private DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dismiss();
        }
    };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        setCancelable(false);
        builder.setTitle(Constants.LOGOUT)
                .setMessage(Constants.LOGOUT_QUESTION)
                .setPositiveButton(Constants.YES, yesListener)
                .setNegativeButton(Constants.NO, noListener);

        return builder.create();

    }
}
