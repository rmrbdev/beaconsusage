package com.example.internship.accenture.beaconapp.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.internship.accenture.beaconapp.Constants;
import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class AddUserDialog extends DialogFragment {


    private EditText usernameEditText;
    private Spinner spinner;

    private String username;
    private String ocupation;

    private DatabaseReference database;
    private List<User> userList;


    public AddUserDialog() {
        super();
        initFirebase();

    }

    public void setUsers(List<User> users) {
        userList = new ArrayList<>(users);
    }

    private DialogInterface.OnClickListener addListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            username = usernameEditText.getText().toString();
            addUser(username, ocupation);
        }
    };

    private DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            username = "";
            ocupation = "";
            dismiss();
        }
    };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_user_dialog, null);

        initUsernameEditText(view);
        initSpinner(view);

        dialog.setTitle(Constants.ADD_NEW_USER)
                .setView(view)
                .setPositiveButton(Constants.ADD, addListener)
                .setNegativeButton(Constants.CANCEL, cancelListener);


        return dialog.create();

    }

    private void initFirebase() {
        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
    }

    private void initUsernameEditText(View view) {
        usernameEditText = view.findViewById(R.id.addUserDialogUsername);
    }

    private void initSpinner(View view) {

        spinner = view.findViewById(R.id.spinnerOcupationOptions);
        spinner.setPrompt(Constants.SELECT_POSITION);

        ArrayAdapter<CharSequence> spinnerAdapter =
                ArrayAdapter.createFromResource(getContext(), R.array.ocupations, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ocupation = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void addUser(String userName, String userOcupation) {

        if (!TextUtils.isEmpty(userName)) {
            if (userName.matches(Constants.USERNAME_VALIDATION_PATTERN)) {
                if (!userIsInBD(userName)) {
                    addUserToFirebase(userName, userOcupation);
                } else {
                    Toast.makeText(getContext(), Constants.USERNAME_EXISTS, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), Constants.BAD_USERNAME, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), Constants.ADD_USERNAME, Toast.LENGTH_SHORT).show();
        }

    }

    private boolean userIsInBD(String userName) {
        for (User user : userList) {
            if (user.getUsername().equals(userName)) {
                return true;
            }
        }
        return false;
    }

    private void addUserToFirebase(String userName, String userOcupation) {
        String id = database.push().getKey();
        User user = new User(id, userName, false, userOcupation);
        database.child(id).setValue(user);
        Toast.makeText(getActivity(), "User " + userName + " has been added successfully", Toast.LENGTH_SHORT).show();

    }


}
