package com.example.internship.accenture.beaconapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.internship.accenture.beaconapp.fragments.HealthFragment;
import com.example.internship.accenture.beaconapp.fragments.SearchSignalFragment;
import com.example.internship.accenture.beaconapp.fragments.VideoFragment;
import com.example.internship.accenture.beaconapp.utils.ContentReceiver;
import com.example.internship.accenture.beaconapp.utils.LogoutDialog;
import com.onyxbeacon.OnyxBeaconApplication;
import com.onyxbeacon.OnyxBeaconManager;
import com.onyxbeacon.listeners.OnyxBeaconsListener;
import com.onyxbeacon.rest.auth.util.AuthData;
import com.onyxbeacon.rest.auth.util.AuthenticationMode;
import com.onyxbeaconservice.Beacon;
import com.onyxbeaconservice.IBeacon;

import java.util.List;

public class MainActivity extends ActivityManager implements OnyxBeaconsListener {

    private String userID;

    private static final String LOG_OUT_DIALOG = "LogOutDialog";

    private OnyxBeaconManager manager;
    private ContentReceiver contentReceiver;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSharedPreferances();
        userID = sharedPreferences.getString(Constants.USER_ID, "");
        initOnyxManager();
        initUI(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        changeToSearchSignalFragment();
    }

    private void initSharedPreferances() {
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
    }


    private void initOnyxManager() {

        manager = OnyxBeaconApplication.getOnyxBeaconManager(this);
        manager.setAPIEndpoint(Constants.ONYX_APY_ENDPOINT);
        manager.setAPIContentEnabled(true);
        manager.enableGeofencing(true);
        manager.setLocationTrackingEnabled(true);

        AuthData authData = new AuthData();
        authData.setAuthenticationMode(AuthenticationMode.CLIENT_SECRET_BASED);
        authData.setClientId(Constants.ONYX_CLIENT_ID);
        authData.setSecret(Constants.ONYX_SECRET);
        manager.setAuthData(authData);

    }

    private void initUI(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Adding default fragment
        if (findViewById(R.id.fragment) != null) {
            if (savedInstanceState != null) {
                return;
            }
            initMainFragment();
        }
    }

    private void initMainFragment() {
        SearchSignalFragment fragmentSearch = new SearchSignalFragment();
        FragmentManager fM = getSupportFragmentManager();
        fM.beginTransaction().add(R.id.fragment, fragmentSearch).commit();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.team_button) {
            Intent intent = new Intent(this, TeamActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeToHealthFragment() {
        Flags.healthFragmentAppeared = true;
        Flags.videoFragmentAppeared = false;
        Flags.logoutFragmentAppeared = false;
        Flags.searchSignalFragmentAppeared = false;

        Fragment fragment = new HealthFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment);
        ft.addToBackStack(null).commit();
    }


    public void changeToSearchSignalFragment() {
        Flags.healthFragmentAppeared = false;
        Flags.videoFragmentAppeared = false;
        Flags.logoutFragmentAppeared = false;
        Flags.searchSignalFragmentAppeared = true;

        Fragment fragment = new SearchSignalFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment);
        ft.commit();
    }


    public void changeToVideoFragment() {
        Flags.videoFragmentAppeared = true;
        Flags.healthFragmentAppeared = true;
        Flags.logoutFragmentAppeared = true;
        Flags.searchSignalFragmentAppeared = true;

        Fragment fragment = new VideoFragment();
        FragmentManager fM = getSupportFragmentManager();
        FragmentTransaction fT = fM.beginTransaction();
        fT.replace(R.id.fragment, fragment).commit();
    }


    public void onResume() {
        super.onResume();
        manager.setForegroundMode(true);
        if (contentReceiver == null) contentReceiver = ContentReceiver.getInstance();
        contentReceiver.setOnyxBeaconsListener(this);
        this.registerReceiver(contentReceiver, new IntentFilter(Constants.ONYX_CONTENT_RECEIVER));

    }

    public void onPause() {
        super.onPause();
        manager.setForegroundMode(false);
        unregisterReceiver(contentReceiver);
    }


    @Override
    public void didRangeBeaconsInRegion(List<Beacon> list) {
        for (Beacon beacon : list) {
            if (beacon instanceof IBeacon) {

                IBeacon iBeacon = (IBeacon) beacon;
                Log.i("TEST:", iBeacon.getMinor() + " : " + iBeacon.getAccuracy());


                switch (iBeacon.getMinor()) {
                    case 6714:
                        if (iBeacon.getAccuracy() <= 0.2 && !Flags.healthFragmentAppeared) {
                            changeToHealthFragment();
                        } else {
                            if (!Flags.searchSignalFragmentAppeared) {
                                changeToSearchSignalFragment();
                            }
                        }
                        break;
                    case 6689:
                        if (iBeacon.getAccuracy() <= 0.2 && !Flags.videoFragmentAppeared) {
                            changeToVideoFragment();
                        }
                        break;
                    case 6657:
                        if (iBeacon.getAccuracy() <= 0.2 && !Flags.logoutFragmentAppeared) {
                            showLogoutDialogFragment();
                        }
                        break;

                }
            }
        }
    }

    public void showLogoutDialogFragment() {
        Flags.logoutFragmentAppeared = true;
        Flags.healthFragmentAppeared = false;
        Flags.videoFragmentAppeared = false;
        Flags.searchSignalFragmentAppeared = false;

        LogoutDialog alertDialog = new LogoutDialog();
        alertDialog.setUserID(userID);
        alertDialog.show(getSupportFragmentManager(), LOG_OUT_DIALOG);
    }
}
