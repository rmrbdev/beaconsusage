package com.example.internship.accenture.beaconapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.internship.accenture.beaconapp.Constants;
import com.example.internship.accenture.beaconapp.Flags;
import com.example.internship.accenture.beaconapp.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;


public class VideoFragment extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video, container, false);

        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();


        final YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {
            }

            @Override
            public void onLoaded(String s) {
            }

            @Override
            public void onAdStarted() {
            }

            @Override
            public void onVideoStarted() {
                Flags.videoFragmentAppeared = true;
                Flags.healthFragmentAppeared = true;
                Flags.logoutFragmentAppeared = true;
                Flags.searchSignalFragmentAppeared = true;
            }

            @Override
            public void onVideoEnded() {
                changeToSearchSignalFragment();
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        };


        youTubePlayerFragment.initialize(Constants.YOUTUBE_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    player.loadVideo(Constants.YOUTUBE_VIDEO_URL);
                    player.setPlayerStateChangeListener(playerStateChangeListener);
                    player.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                // YouTube error
                String errorMessage = error.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
            }
        });

        return rootView;
    }


    public void changeToSearchSignalFragment() {
        Flags.healthFragmentAppeared = false;
        Flags.videoFragmentAppeared = false;
        Flags.logoutFragmentAppeared = false;
        Flags.searchSignalFragmentAppeared = true;

        Fragment fragment = new SearchSignalFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment);
        ft.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Flags.videoFragmentAppeared = false;
        Flags.healthFragmentAppeared = false;
        Flags.logoutFragmentAppeared = false;
        Flags.searchSignalFragmentAppeared = false;
        Flags.onStoppedFlag = false;

    }
}
