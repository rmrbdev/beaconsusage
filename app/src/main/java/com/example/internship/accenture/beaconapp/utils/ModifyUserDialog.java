package com.example.internship.accenture.beaconapp.utils;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.internship.accenture.beaconapp.Constants;
import com.example.internship.accenture.beaconapp.R;
import com.example.internship.accenture.beaconapp.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class ModifyUserDialog extends DialogFragment {


    private EditText usernameEditText;
    private Spinner spinner;
    private String userID;
    private String username;
    private boolean isOnline;
    private List<User> userList;

    private String occupation;
    private DatabaseReference database;


    public ModifyUserDialog() {
        super();
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUsers(List<User> users) {
        userList = new ArrayList<>(users);
    }


    private DialogInterface.OnClickListener modifyListenter = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            username = usernameEditText.getText().toString();
            modifyUser(username, occupation);
        }
    };

    private DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            username = "";
            occupation = "";
            dismiss();
        }
    };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_user_dialog, null);

        initUsernameEditText(view);

        initSpinner(view);

        dialog.setTitle(Constants.MODIFY_USER)
                .setView(view)
                .setPositiveButton(Constants.UPDATE, modifyListenter)
                .setNegativeButton(Constants.CANCEL, cancelListener);


        return dialog.create();

    }


    private void initFirebase() {
        database = FirebaseDatabase.getInstance().getReference(Constants.USERS);
        
    }

    private void initUsernameEditText(View view) {
        usernameEditText = view.findViewById(R.id.addUserDialogUsername);
        usernameEditText.setText(username);
    }

    private void initSpinner(View view) {

        spinner = view.findViewById(R.id.spinnerOcupationOptions);
        spinner.setPrompt(Constants.SELECT_POSITION);


        ArrayAdapter<CharSequence> spinnerAdapter =
                ArrayAdapter.createFromResource(getContext(), R.array.ocupations, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setSelection(spinnerAdapter.getPosition(occupation));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                occupation = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private boolean userIsInBD(String userName) {
        for (User user : userList) {
            if (user.getUsername().equals(userName)) {
                return true;
            }
        }
        return false;
    }

    private void modifyUser(String userName, String userOcupation) {
        initFirebase();
        if (!TextUtils.isEmpty(userName)) {
            if (userName.matches(Constants.USERNAME_VALIDATION_PATTERN)) {
                if (!userIsInBD(userName)) {
                    modifyUserInFirebase(userName, userOcupation);
                } else {
                    Toast.makeText(getContext(), Constants.USERNAME_EXISTS, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), Constants.BAD_USERNAME, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), Constants.ADD_USERNAME, Toast.LENGTH_SHORT).show();
        }

    }

    private void modifyUserInFirebase(String userName, String userOcupation) {
        String id = userID;
        User user = new User(id, userName, isOnline, userOcupation);
        database.child(id).setValue(user);
        Toast.makeText(getActivity(), Constants.UPDATE_SUCCESSFULLY, Toast.LENGTH_SHORT).show();

    }
}

